#ifndef SEARCHWORKER_H
#define SEARCHWORKER_H

#include <QFileInfo>
#include <QObject>
#include <QThread>

class SearchWorker : public QObject {
    Q_OBJECT
public:
    explicit SearchWorker(QString const& dir);

signals:
    void pushOutGroupe(QVector<QFileInfo> group);
    void pushOutBadFile(QFileInfo);
    void finished();
    void searchProgress(int value);
    void presentForWatcher(QVector<QFileInfo> fileInfoVector);

public slots:
    void process();

private:
    QString dir;
    qint64 progress;
    qint64 fullProgress;

    void splitGroup(QVector<QFileInfo> const& group);

    inline bool needStop() { return QThread::currentThread()->isInterruptionRequested(); }
};

#endif // SEARCHWORKER_H
