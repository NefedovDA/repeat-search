#include "searchworker.h"

#include <QCryptographicHash>
#include <QDirIterator>
#include <QThread>
#include <QVector>

SearchWorker::SearchWorker(QString const& dir)
    : QObject()
    , dir(dir)
    , progress(0)
    , fullProgress(0)
{
}

void SearchWorker::process()
{
    QDirIterator it(dir, QDir::Files | QDir::NoSymLinks, QDirIterator::Subdirectories);
    QVector<QFileInfo> fileInfoVector;
    while (it.hasNext()) {
        QFileInfo fileInfo(it.next());
        fullProgress += fileInfo.size();
        fileInfoVector.append(fileInfo);
    }

    QDirIterator it_(dir, QDir::Dirs | QDir::NoSymLinks, QDirIterator::Subdirectories);
    QVector<QFileInfo> fileInfoVector_;
    while (it_.hasNext()) {
        QFileInfo fileInfo(it_.next());
        fileInfoVector_.append(fileInfo);
    }

    emit presentForWatcher(fileInfoVector);
    emit presentForWatcher(fileInfoVector_);

    if (needStop()) {
        emit finished();
        return;
    }

    std::sort(fileInfoVector.begin(), fileInfoVector.end(),
        [](QFileInfo const& left, QFileInfo const& right) { return left.size() > right.size(); });

    qint64 currentSize;
    auto itEnd = fileInfoVector.end();

    for (auto it = fileInfoVector.begin(); it != itEnd;) {
        if (needStop()) {
            emit finished();
            return;
        }

        currentSize = it->size();
        QVector<QFileInfo> group;

        for (; it != itEnd && it->size() == currentSize; ++it, progress += currentSize) {
            group.append(*it);
        }

        if (group.size() != 1) {
            splitGroup(group);
        }
        emit searchProgress(progress * 100 / fullProgress);
    }

    emit finished();
}

void SearchWorker::splitGroup(QVector<QFileInfo> const& group)
{
    qint64 size = group.size();
    QCryptographicHash sha(QCryptographicHash::Sha3_256);
    QVector<QByteArray> serialNmber(size);
    QVector<qint64> index;

    for (qint64 passerby = 0; passerby < size; ++passerby) {
        sha.reset();
        QFile file(group[passerby].absoluteFilePath());
        if (file.open(QIODevice::ReadOnly) && sha.addData(&file)) {
            serialNmber[passerby] = sha.result();
            index.append(passerby);
        } else {
            // could not open or read file
            emit pushOutBadFile(group[passerby]);
        }
    }

    std::sort(index.begin(), index.end(), [&serialNmber](qint64 left, qint64 right) { return serialNmber[left] < serialNmber[right]; });

    QByteArray* currentHash;
    auto itEnd = index.end();
    for (auto it = index.begin(); it != itEnd;) {
        if (needStop()) {
            emit finished();
            return;
        }

        QVector<QFileInfo> sendableGroup;
        currentHash = &serialNmber[*it];
        for (; it != itEnd && *currentHash == serialNmber[*it]; ++it) {
            sendableGroup.append(group[*it]);
        }

        if (sendableGroup.size() != 1) {
            emit pushOutGroupe(sendableGroup);
        }
    }
}
