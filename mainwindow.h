#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QFileInfo>
#include <QFileSystemWatcher>
#include <QTreeWidgetItem>
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

public slots:
    void appendTreeWidget(QVector<QFileInfo> groups);
    void appendExceptionFiles(QFileInfo fileInfo);
    void addInWatcher(QVector<QFileInfo> fileInfoVector);

private slots:
    void selectDirectory();
    void scanDirectory();
    void showAboutDialog();
    void clearTreeWidget();
    void research();

    void showContextMenu(QPoint const& position);
    void findContextForOpen();
    void openItemFile(QTreeWidgetItem* item);
    void deleteFile();
    void copyPath();

    void showEndInfo();
    void wakeUp();
    void setNeedResearch(QString);

private:
    std::unique_ptr<Ui::MainWindow> ui;
    QTreeWidgetItem* warningsBufferItem;
    QString dir;
    std::shared_ptr<QFileSystemWatcher> directoryWatcher;
    bool needResearch;

    void startSearch(QString const& dir);

    template <typename ParentT>
    static QTreeWidgetItem* createItem(ParentT* parent, QString const& fild0, QString const& fild1)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem(parent);
        item->setText(0, fild0);
        item->setText(1, fild1);
        return item;
    }

    QTreeWidgetItem* getSelectedItem();

    static const QString UNITS[5];
    QString formatFileSize(double size);

    void resetDirectoryWatcher();
};

#endif // MAINWINDOW_H
