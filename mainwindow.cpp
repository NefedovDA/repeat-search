#include "mainwindow.h"
#include "searchworker.h"
#include "ui_mainwindow.h"

#include <QClipboard>
#include <QCommonStyle>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>

#include <QLabel>
#include <QMetaType>
#include <QThread>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , warningsBufferItem(nullptr)
    , dir()
    , directoryWatcher()
    , needResearch()
{
    ui->setupUi(this);
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry()));

    ui->treeWidget->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->treeWidget->header()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    QCommonStyle style;
    ui->actionSelectDirectory->setIcon(style.standardIcon(QCommonStyle::SP_DialogOpenButton));
    ui->actionExit->setIcon(style.standardIcon(QCommonStyle::SP_DialogCloseButton));
    ui->actionAbout->setIcon(style.standardIcon(QCommonStyle::SP_DialogHelpButton));
    ui->actionClear->setIcon(style.standardIcon(QCommonStyle::SP_DialogResetButton));
    ui->actionResearch->setIcon(style.standardIcon(QCommonStyle::SP_BrowserReload));

    connect(ui->actionSelectDirectory, &QAction::triggered, this, &MainWindow::selectDirectory);
    connect(ui->actionExit, &QAction::triggered, this, &QWidget::close);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::showAboutDialog);
    connect(ui->actionClear, &QAction::triggered, this, &MainWindow::clearTreeWidget);
    connect(ui->actionResearch, &QAction::triggered, this, &MainWindow::research);

    connect(ui->treeWidget, &QTreeWidget::itemDoubleClicked, this, &MainWindow::openItemFile);
    connect(ui->treeWidget, &QTreeWidget::customContextMenuRequested, this, &MainWindow::showContextMenu);

    qRegisterMetaType<QVector<QFileInfo>>("QVector<QFileInfo>");
}

MainWindow::~MainWindow()
{
}

void MainWindow::appendTreeWidget(QVector<QFileInfo> group)
{
    QTreeWidgetItem* topLevelItem = createItem(ui->treeWidget,
        group.front().fileName(), QString("%1 items").arg(group.size()));

    for (QFileInfo const& fileInfo : group) {
        topLevelItem->addChild(createItem(topLevelItem,
            fileInfo.absoluteFilePath(), formatFileSize(static_cast<double>(fileInfo.size()))));
    }

    ui->treeWidget->addTopLevelItem(topLevelItem);
}

void MainWindow::appendExceptionFiles(QFileInfo fileInfo)
{
    warningsBufferItem->addChild(createItem(warningsBufferItem,
        fileInfo.absoluteFilePath(), formatFileSize(static_cast<double>(fileInfo.size()))));
}

void MainWindow::addInWatcher(QVector<QFileInfo> fileInfoVector)
{
    for (QFileInfo const& fileInfo : fileInfoVector) {
        directoryWatcher->addPath(fileInfo.absoluteFilePath());
    }
}

void MainWindow::selectDirectory()
{
    dir = QFileDialog::getExistingDirectory(this, "Select Directory for Scanning", QString(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    resetDirectoryWatcher();

    scanDirectory();
}

void MainWindow::scanDirectory()
{
    setWindowTitle(QString("Directory content - %1").arg(dir));
    clearTreeWidget();
    startSearch(dir);
}

void MainWindow::showAboutDialog()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::clearTreeWidget()
{
    ui->treeWidget->clear();
}

void MainWindow::research()
{
    if (needResearch) {
        needResearch = false;
        scanDirectory();
    } else {
        QMessageBox::information(this, "Research", "Directory did not changed.", QMessageBox::Ok);
    }
}

void MainWindow::showContextMenu(QPoint const& position)
{
    QTreeWidgetItem* item = getSelectedItem();
    if (item == nullptr || item->parent() == nullptr) {
        return;
    }

    QMenu* menu = new QMenu(this);

    QAction* openAction = new QAction("Open file", this);
    QAction* copyAction = new QAction("Copy path", this);
    QAction* deleteAction = new QAction("Delete file", this);

    connect(openAction, &QAction::triggered, this, &MainWindow::findContextForOpen);
    connect(copyAction, &QAction::triggered, this, &MainWindow::copyPath);
    connect(deleteAction, &QAction::triggered, this, &MainWindow::deleteFile);

    menu->addAction(openAction);
    menu->addAction(copyAction);
    menu->addAction(deleteAction);

    menu->popup(ui->treeWidget->viewport()->mapToGlobal(position));
}

void MainWindow::findContextForOpen()
{
    openItemFile(getSelectedItem());
}

void MainWindow::openItemFile(QTreeWidgetItem* item)
{
    if (item->parent() != nullptr) {
        QString path = item->text(0);
        if (QFileInfo(path).isExecutable()) {
            QMessageBox::warning(this, "Opening file", "File could not be opened.", QMessageBox::Ok);
            return;
        }
        bool success = QDesktopServices::openUrl(QUrl::fromLocalFile(path));
        if (!success) {
            QMessageBox::warning(this, "Opening file", "File could not be opened.", QMessageBox::Ok);
        }
    }
}

void MainWindow::deleteFile()
{
    QTreeWidgetItem* item = getSelectedItem();
    QString path = item->text(0);

    QString dialogWindowTitle("Deleting file");

    if (QMessageBox::warning(this, dialogWindowTitle, "Are you sure you want to delete this file?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes) {
        if (QFile::remove(path)) {
            item->setSelected(false);
            item->setDisabled(true);
            QMessageBox::information(this, dialogWindowTitle, "File was deleted successfully.", QMessageBox::Ok);
        } else {
            QMessageBox::warning(this, dialogWindowTitle, "Could not delete file.", QMessageBox::Ok);
        }
    }
}

void MainWindow::copyPath()
{
    QTreeWidgetItem* item = getSelectedItem();
    QString path = item->text(0);

    QApplication::clipboard()->setText(path);
}

void MainWindow::showEndInfo()
{
    QString message("Search finished.\n");
    qint64 groupsCount = ui->treeWidget->topLevelItemCount() - 1;
    switch (groupsCount) {
    case 0:
        message.append("No equals files in this directory.");
        break;
    case 1:
        message.append("1 group of equals files was founded.");
        break;
    default:
        message.append(QString::number(groupsCount)).append(" groups of equals files were founded.");
        break;
    }

    if (warningsBufferItem->childCount() != 0) {
        warningsBufferItem->setHidden(false);
        warningsBufferItem->setText(1, QString::number(warningsBufferItem->childCount()) + " items");
    }

    QMessageBox dialog;
    dialog.setWindowTitle("Info");
    dialog.setText(message);
    dialog.setStandardButtons(QMessageBox::Ok);

    dialog.exec();
}

void MainWindow::wakeUp()
{
    setEnabled(true);
}

void MainWindow::setNeedResearch(QString)
{
    needResearch = true;
}

void MainWindow::startSearch(QString const& dir)
{
    SearchWorker* worker = new SearchWorker(dir);
    QThread* thread = new QThread();
    worker->moveToThread(thread);

    connect(thread, &QThread::started, worker, &SearchWorker::process);
    connect(worker, &SearchWorker::pushOutGroupe, this, &MainWindow::appendTreeWidget);
    connect(worker, &SearchWorker::finished, thread, &QThread::quit);
    connect(worker, &SearchWorker::finished, this, &MainWindow::wakeUp);

    QProgressDialog* progressDialog = new QProgressDialog("Searching the repeats...", "&Cancel", 0, 100);
    progressDialog->setMinimumDuration(0);
    progressDialog->setWindowTitle("Please Wait");

    connect(worker, &SearchWorker::searchProgress, progressDialog, &QProgressDialog::setValue);
    connect(progressDialog, &QProgressDialog::canceled, thread, &QThread::requestInterruption);

    connect(worker, &SearchWorker::finished, worker, &SearchWorker::deleteLater);
    connect(worker, &SearchWorker::finished, progressDialog, &QProgressDialog::deleteLater);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    warningsBufferItem = new QTreeWidgetItem(ui->treeWidget);
    warningsBufferItem->setHidden(true);
    warningsBufferItem->setText(0, "Some files could not be read");
    ui->treeWidget->addTopLevelItem(warningsBufferItem);

    connect(worker, &SearchWorker::presentForWatcher, this, &MainWindow::addInWatcher);

    connect(worker, &SearchWorker::pushOutBadFile, this, &MainWindow::appendExceptionFiles);
    connect(worker, &SearchWorker::finished, this, &MainWindow::showEndInfo);

    setEnabled(false);
    thread->start();
}

QTreeWidgetItem* MainWindow::getSelectedItem()
{
    auto const& list = ui->treeWidget->selectedItems();
    if (list.size() == 0) {
        return nullptr;
    } else {
        return *list.begin();
    }
}

const QString MainWindow::UNITS[5] = { "bytes", "Kb", "Mb", "Gb", "Tb" };

QString MainWindow::formatFileSize(double size)
{
    qint32 lenght = sizeof(MainWindow::UNITS) / sizeof(QString);
    qint32 ind;
    for (ind = 0; ind < lenght - 1; ++ind, size /= 1024) {
        if (size < 1024) {
            break;
        }
    }
    return QString::number(size, 'f', 1) + " " + MainWindow::UNITS[ind];
}

void MainWindow::resetDirectoryWatcher()
{
    needResearch = false;
    directoryWatcher = std::make_shared<QFileSystemWatcher>(new QFileSystemWatcher);
    connect(directoryWatcher.get(), &QFileSystemWatcher::directoryChanged, this, &MainWindow::setNeedResearch);
    connect(directoryWatcher.get(), &QFileSystemWatcher::fileChanged, this, &MainWindow::setNeedResearch);
}
